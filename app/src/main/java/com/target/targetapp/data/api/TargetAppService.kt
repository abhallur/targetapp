package com.target.targetapp.data.api

import com.target.targetapp.data.model.response.githubtrending.GithubTrendingRepoItem
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface TargetAppService {

    @GET("developers")
    fun callGetRepositoryItemsApi(@Query("language") language: String, @Query("since") since: String = "weekly")
            : Single<ArrayList<GithubTrendingRepoItem?>?>

}