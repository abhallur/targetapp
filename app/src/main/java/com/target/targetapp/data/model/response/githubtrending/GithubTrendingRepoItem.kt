package com.target.targetapp.data.model.response.githubtrending

import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class GithubTrendingRepoItem(

    @SerializedName("username")
    var username: String? = null,

    @SerializedName("name")
    var name: String? = null,

    @SerializedName("url")
    var url: String? = null,

    @SerializedName("avatar")
    var avatar: String? = null,

    @SerializedName("repo")
    var repo: Repo? = null

) : Serializable