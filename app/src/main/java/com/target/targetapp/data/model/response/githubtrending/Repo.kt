package com.target.targetapp.data.model.response.githubtrending

import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class Repo(

    @SerializedName("name")
    var name: String? = null,

    @SerializedName("url")
    var url: String? = null,

    @SerializedName("description")
    var description: String? = null

) : Serializable