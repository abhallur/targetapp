package com.target.targetapp.di.component

import com.target.targetapp.application.TargetApp
import com.target.targetapp.di.module.ActivityModule
import com.target.targetapp.di.module.AppModule
import com.target.targetapp.di.module.NetworkModule
import com.target.targetapp.di.module.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        ActivityModule::class,
        AndroidInjectionModule::class,
        AndroidSupportInjectionModule::class,
        AppModule::class,
        NetworkModule::class,
        ViewModelModule::class
    ]
)
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(app: TargetApp): Builder

        fun build(): AppComponent
    }

    fun inject(app: TargetApp)
}