package com.target.targetapp.di.module

import com.target.targetapp.di.scope.ActivityScope
import com.target.targetapp.ui.repositorydetails.RepositoryDetailsActivity
import com.target.targetapp.ui.trendingrepositories.TrendingRepositoriesActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeTrendingRepositoriesActivity(): TrendingRepositoriesActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeRepositoryDetailsActivity(): RepositoryDetailsActivity

}