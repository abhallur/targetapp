package com.target.targetapp.di.module

import android.content.Context
import com.target.targetapp.application.TargetApp
import com.target.targetapp.utils.Utils
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideContext(): Context {
        return TargetApp.instance
    }

    @Provides
    @Singleton
    fun provideUtils(context: Context): Utils {
        return Utils(context)
    }
}