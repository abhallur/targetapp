package com.target.targetapp.di.module

import android.os.Environment
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.target.targetapp.BuildConfig
import com.target.targetapp.data.api.TargetAppService
import com.target.targetapp.ui.trendingrepositories.TrendingRepositoryService
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .cache(Cache(Environment.getDownloadCacheDirectory(), 10 * 1024 * 1024))
            .build()
    }

    @Provides
    @Singleton
    fun provideGson(): Gson {
        return GsonBuilder()
            .create()
    }

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.API_BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    @Provides
    @Singleton
    fun provideTrendingRepositoryService(targetAppService: TargetAppService): TrendingRepositoryService {
        return TrendingRepositoryService(targetAppService)
    }

    @Provides
    @Singleton
    fun provideTargetAppService(retrofit: Retrofit): TargetAppService =
        retrofit.create(TargetAppService::class.java)

}