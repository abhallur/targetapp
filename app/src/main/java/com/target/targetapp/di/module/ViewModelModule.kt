package com.target.targetapp.di.module

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.target.targetapp.di.TargetAppViewModelFactory
import com.target.targetapp.di.scope.ViewModelKey
import com.target.targetapp.ui.repositorydetails.RepositoryDetailsVM
import com.target.targetapp.ui.trendingrepositories.TrendingRepositoriesVM
import com.target.targetapp.ui.trendingrepositories.TrendingRepositoryItemVM
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
internal abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: TargetAppViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(TrendingRepositoriesVM::class)
    abstract fun bindTrendingRepositoriesVM(trendingRepositoriesVM: TrendingRepositoriesVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TrendingRepositoryItemVM::class)
    abstract fun bindTrendingRepositoryItemVM(trendingRepositoryItemVM: TrendingRepositoryItemVM): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RepositoryDetailsVM::class)
    abstract fun bindRepositoryDetailsVM(repositoryDetailsVM: RepositoryDetailsVM): ViewModel

}