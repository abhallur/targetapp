package com.target.targetapp.ui.base

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import dagger.android.AndroidInjection
import dagger.android.DispatchingAndroidInjector
import javax.inject.Inject

abstract class BaseActivity<DB : ViewDataBinding, VM : ViewModel> : AppCompatActivity() {

    lateinit var viewModel: VM
    private lateinit var viewDataBinding: DB

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory


    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        performBinding()
    }

    private fun performBinding() {
        viewDataBinding = DataBindingUtil.setContentView(this, layoutRes)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(viewModel())
        viewDataBinding.setVariable(bindingVariable(), viewModel)
        viewDataBinding.executePendingBindings()
    }


    /**
     * @return layout resource id
     */
    @get:LayoutRes
    abstract val layoutRes: Int

    abstract fun viewModel(): Class<VM>

    abstract fun bindingVariable(): Int

}