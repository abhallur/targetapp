package com.target.targetapp.ui.base

import android.arch.lifecycle.ViewModel
import android.databinding.ObservableField
import com.target.targetapp.utils.Utils
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

open class BaseViewModel : ViewModel() {

    @Inject
    lateinit var utils: Utils

    val loading = ObservableField<Boolean>()

    var mCompositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onCleared() {
        mCompositeDisposable.dispose()
        super.onCleared()
    }

    fun getString(stringResId: Int): String {
        return utils.getMessage(stringResId)
    }

    fun hasConnection(): Boolean {
        return utils.hasConnection()
    }
}