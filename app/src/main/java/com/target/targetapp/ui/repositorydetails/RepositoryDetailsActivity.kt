package com.target.targetapp.ui.repositorydetails

import android.os.Bundle
import com.target.targetapp.BR
import com.target.targetapp.R
import com.target.targetapp.data.model.response.githubtrending.GithubTrendingRepoItem
import com.target.targetapp.databinding.ActivityRepositoryDetailsBinding
import com.target.targetapp.ui.base.BaseActivity
import kotlinx.android.synthetic.main.top_bar.*

class RepositoryDetailsActivity : BaseActivity<ActivityRepositoryDetailsBinding, RepositoryDetailsVM>() {

    override val layoutRes: Int
        get() = R.layout.activity_repository_details

    override fun viewModel(): Class<RepositoryDetailsVM> = RepositoryDetailsVM::class.java

    override fun bindingVariable(): Int = BR.viewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initData()
        setClickListeners()
    }

    private fun initData() {
        topBarLabel.text = getString(R.string.text_repository_details)
        viewModel.setData(intent.getSerializableExtra(getString(R.string.key_repo_item)) as GithubTrendingRepoItem)
    }

    private fun setClickListeners() {
        backArrow.setOnClickListener { onBackPressed() }
    }
}
