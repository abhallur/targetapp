package com.target.targetapp.ui.repositorydetails

import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.target.targetapp.data.model.response.githubtrending.GithubTrendingRepoItem
import com.target.targetapp.ui.base.BaseViewModel
import com.target.targetapp.utils.TransitionImage
import javax.inject.Inject

class RepositoryDetailsVM @Inject constructor() : BaseViewModel() {


    val userName = MutableLiveData<String>()
    val name = MutableLiveData<String>()
    val githubUrl = MutableLiveData<String>()
    val avatar = MutableLiveData<String>()
    val repoName = MutableLiveData<String>()
    val repoUrl = MutableLiveData<String>()
    val description = MutableLiveData<String>()

    init {
        avatar.value = TransitionImage.avatar
    }

    fun setData(item: GithubTrendingRepoItem) {
        avatar.value = item.avatar
        userName.value = item.username
        name.value = item.name
        githubUrl.value = item.url
        repoName.value = item.repo?.name
        repoUrl.value = item.repo?.url
        description.value = item.repo?.description
    }

}