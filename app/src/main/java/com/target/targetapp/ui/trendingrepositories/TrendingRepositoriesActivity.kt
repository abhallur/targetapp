package com.target.targetapp.ui.trendingrepositories

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.widget.Toast
import com.target.targetapp.BR
import com.target.targetapp.R
import com.target.targetapp.databinding.ActivityTrendingRepositoriesBinding
import com.target.targetapp.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_trending_repositories.*
import kotlinx.android.synthetic.main.top_bar.*

class TrendingRepositoriesActivity : BaseActivity<ActivityTrendingRepositoriesBinding, TrendingRepositoriesVM>(),
    SwipeRefreshLayout.OnRefreshListener {

    override val layoutRes: Int
        get() = R.layout.activity_trending_repositories

    override fun viewModel(): Class<TrendingRepositoriesVM> = TrendingRepositoriesVM::class.java

    override fun bindingVariable(): Int = BR.viewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initData()
    }

    override fun onResume() {
        swipeToRefresh.isRefreshing = true
        viewModel.loadItems()
        super.onResume()
    }

    override fun onRefresh() {
        viewModel.loadItems()
    }


    private fun initData() {
        topBarLabel.text = getString(R.string.text_trending_repositories)
        setObservers()
        setClickListeners()
        swipeToRefresh.setOnRefreshListener(this)
    }

    private fun setObservers() {
        viewModel.showInternetError.observe(this, Observer {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        })

        viewModel.loadingDone.observe(this, Observer {
            if (it == true) swipeToRefresh.isRefreshing = false
        })
    }

    private fun setClickListeners() {
        backArrow.setOnClickListener { onBackPressed() }
    }
}
