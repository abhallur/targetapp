package com.target.targetapp.ui.trendingrepositories

import android.arch.lifecycle.MutableLiveData
import android.databinding.ObservableField
import com.target.targetapp.R
import com.target.targetapp.data.model.response.githubtrending.GithubTrendingRepoItem
import com.target.targetapp.ui.base.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class TrendingRepositoriesVM @Inject constructor(private val trendingRepositoryService: TrendingRepositoryService) :
    BaseViewModel() {

    val adapter = ObservableField<TrendingRepositoryItemAdapter>()
    val showError = ObservableField<Boolean>()
    val errorText = MutableLiveData<String>()
    val loadingDone = MutableLiveData<Boolean>()
    val showInternetError = MutableLiveData<String>()

    init {
        showError.set(false)
        adapter.set(TrendingRepositoryItemAdapter())
    }

    fun loadItems() {
        if (hasConnection()) {
            loading.set(true)
            mCompositeDisposable.add(
                trendingRepositoryService.execute()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doFinally {
                        loading.set(false)
                        loadingDone.value = true
                    }
                    .subscribe({ listOfItems ->
                        listOfItems?.let {
                            setAdapter(it)
                        } ?: run {
                            showNoItemsError(getString(R.string.text_no_items), true)
                        }
                    }, {
                        showNoItemsError(getString(R.string.error_loading_data), false)
                    })
            )
        } else {
            loading.set(false)
            loadingDone.value = true
            if (adapter.get()?.itemCount!! <= 0)
                showNoItemsError(getString(R.string.error_no_internet), true)
            else
                showInternetError.value = getString(R.string.error_no_internet)
        }

    }

    private fun setAdapter(list: ArrayList<GithubTrendingRepoItem?>) {
        if (list.size > 0) {
            showError.set(false)
            errorText.value = ""
            adapter.get()?.addRepoList(list)
        } else {
            showNoItemsError(getString(R.string.text_no_items), true)
        }
    }

    private fun showNoItemsError(error: String, resetAdapter: Boolean) {
        errorText.value = error
        showError.set(true)
        if (resetAdapter) adapter.get()?.addRepoList(ArrayList())
    }
}