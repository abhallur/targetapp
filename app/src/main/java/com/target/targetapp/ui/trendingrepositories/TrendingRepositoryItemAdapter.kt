package com.target.targetapp.ui.trendingrepositories

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.SystemClock
import android.support.v4.app.ActivityOptionsCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.target.targetapp.R
import com.target.targetapp.data.model.response.githubtrending.GithubTrendingRepoItem
import com.target.targetapp.databinding.InflaterItemTrendingRepositoryBinding
import com.target.targetapp.ui.repositorydetails.RepositoryDetailsActivity
import com.target.targetapp.utils.TransitionImage


class TrendingRepositoryItemAdapter : RecyclerView.Adapter<TrendingRepositoryItemAdapter.ViewHolder>() {

    private lateinit var githubTrendingRepoItemList: ArrayList<GithubTrendingRepoItem?>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrendingRepositoryItemAdapter.ViewHolder {
        val binding: InflaterItemTrendingRepositoryBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.inflater_item_trending_repository,
                parent,
                false
            )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TrendingRepositoryItemAdapter.ViewHolder, position: Int) {
        holder.bind(githubTrendingRepoItemList[position])
    }

    override fun getItemCount(): Int {
        return if (::githubTrendingRepoItemList.isInitialized) githubTrendingRepoItemList.size else 0
    }

    fun addRepoList(itemList: ArrayList<GithubTrendingRepoItem?>) {
        this.githubTrendingRepoItemList = itemList
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: InflaterItemTrendingRepositoryBinding) :
        RecyclerView.ViewHolder(binding.root),
        View.OnClickListener {

        private var mLastClickTime: Long = 0
        private val viewModel = TrendingRepositoryItemVM()

        init {
            binding.root.setOnClickListener(this)
        }

        fun bind(option: GithubTrendingRepoItem?) {
            viewModel.bind(option)
            binding.viewModel = viewModel
        }

        override fun onClick(view: View?) {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return;
            }
            mLastClickTime = SystemClock.elapsedRealtime()
            openDetailsActivity(itemView, githubTrendingRepoItemList[adapterPosition]!!)
        }

        private fun openDetailsActivity(view: View, item: GithubTrendingRepoItem) {
            val intent = Intent((view.context as TrendingRepositoriesActivity), RepositoryDetailsActivity::class.java)
            intent.putExtra((view.context as TrendingRepositoriesActivity).getString(R.string.key_repo_item), item)
            TransitionImage.avatar = item.avatar
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                    (view.context as TrendingRepositoriesActivity),
                    itemView.findViewById(R.id.imageAvatar) as View,
                    (view.context as TrendingRepositoriesActivity).getString(R.string.transition_name_avatar)
                )
                (view.context as TrendingRepositoriesActivity).startActivity(intent, options.toBundle())
            } else {
                (view.context as TrendingRepositoriesActivity).startActivity(intent)
            }
        }
    }

}
