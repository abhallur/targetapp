package com.target.targetapp.ui.trendingrepositories

import android.arch.lifecycle.MutableLiveData
import com.target.targetapp.data.model.response.githubtrending.GithubTrendingRepoItem
import com.target.targetapp.ui.base.BaseViewModel
import javax.inject.Inject

class TrendingRepositoryItemVM @Inject constructor() : BaseViewModel() {

    val avatarImageRes = MutableLiveData<String>()
    val repositoryName = MutableLiveData<String>()
    val userName = MutableLiveData<String>()


    fun bind(item: GithubTrendingRepoItem?) {
        avatarImageRes.value = item?.avatar
        repositoryName.value = item?.repo?.name
        userName.value = item?.username
    }


}