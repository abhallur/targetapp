package com.target.targetapp.ui.trendingrepositories

import com.target.targetapp.data.api.TargetAppService
import com.target.targetapp.data.model.response.githubtrending.GithubTrendingRepoItem
import com.target.targetapp.utils.AppConstants
import io.reactivex.Single
import javax.inject.Inject

class TrendingRepositoryService @Inject constructor(private val targetAppService: TargetAppService) {

    fun execute(): Single<ArrayList<GithubTrendingRepoItem?>?> {
        return targetAppService.callGetRepositoryItemsApi(AppConstants.LANGUAGE_KOTLIN)
    }
}