package com.target.targetapp.utils

object AppConstants {

    const val LANGUAGE_KOTLIN: String = "kotlin"
    const val LANGUAGE_JAVA: String = "java"
}